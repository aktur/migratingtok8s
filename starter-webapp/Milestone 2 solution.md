### Containerize Profile App and Services Using Docker Compose

__Workflow Solutions:__

Step 2. Create Docker container running MySQL; in it, create a database and a user that can read/write for that database
```
docker run -it -d \
      -e MYSQL_USER=empuser \
      -e MYSQL_PASSWORD=password \
      -e MYSQL_DATABASE=profiles \
      -e MYSQL_ALLOW_EMPTY_PASSWORD=true \
      -p 3306:3306 \
      --name mymysql \
      mysql
```

Step 4. Reconfigure the application to use **/data** as the image storage directory. Create Docker image for application.

application.yml:
```
images:
  directory: /data
```
Dockerfile:
```
FROM adoptopenjdk:11-jre-hotspot
ARG JAR_FILE=profile-mvc-1.0.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

Step 5. Run profile application in docker, communicating with the MySQL docker container. Set up persistent storage for the **/data** image directory. Create a docker-compose setup for both containers (this allows you to connect them via service names).

docker-compose.yml (with local image)
```
version: '3.7'

# Define services
services:
  # App backend service
  app-server:
    # Configuration for building the docker image for the backend service
    build:
      context: profile # Use an image built from the specified dockerfile in the `polling-app-server` directory.
      dockerfile: Dockerfile
    ports:
      - "8080:8080" # Forward the exposed port 8080 on the container to port 8080 on the host machine
#    restart: always
    depends_on:
      - db # This service depends on mysql. Start that first.
    environment: # Pass environment variables to the service
      SPRING_DATASOURCE_URL: jdbc:mysql://db:3306/profiles
      SPRING_DATASOURCE_USERNAME: empuser
      SPRING_DATASOURCE_PASSWORD: password
    volumes:
      - images-data:/data
    networks: # Networks to join (Services on the same network can communicate with each other using their name)
      - backend

  # Database Service (Mysql)
  db:
    image: mysql:latest
    ports:
      - "3306:3306"
    restart: always
    environment:
      MYSQL_DATABASE: profiles
      MYSQL_USER: empuser
      MYSQL_PASSWORD: password
#      MYSQL_ROOT_PASSWORD: root
      MYSQL_ALLOW_EMPTY_PASSWORD: "true"
    volumes:
      - db-data:/var/lib/mysql
    networks:
      - backend  

# Volumes
volumes:
  db-data:
  images-data:

# Networks to be created to facilitate communication between containers
networks:
  backend:
```

Step 8. Push the docker image to a repository. [Dockerhub](https://hub.docker.com/) will work fine (you'll need to create a free account), or you can stand up a docker repository like [harbor](https://goharbor.io/)

(commands with console output)
```
➜  profile git:(master) ✗ docker build -t grafpoo/profile-app .
Sending build context to Docker daemon  47.67MB
Step 1/4 : FROM adoptopenjdk:11-jre-hotspot
 ---> 3be5117ed0ca
Step 2/4 : ARG JAR_FILE=profile-mvc-1.0.jar
 ---> Using cache
 ---> 539e0fb06e2e
Step 3/4 : COPY ${JAR_FILE} app.jar
 ---> 0f74ce64a323
Step 4/4 : ENTRYPOINT ["java","-jar","/app.jar"]
 ---> Running in c8e62988e51b
Removing intermediate container c8e62988e51b
 ---> 8bc413c14c33
Successfully built 8bc413c14c33
Successfully tagged grafpoo/profile-app:latest
➜  profile git:(master) ✗ docker push grafpoo/profile-app      
The push refers to repository [docker.io/grafpoo/profile-app]
21b767e6d82f: Pushed
51b73f76f882: Mounted from library/adoptopenjdk
01c4039a9616: Mounted from library/adoptopenjdk
1852b2300972: Mounted from library/adoptopenjdk
03c9b9f537a4: Mounted from library/adoptopenjdk
8c98131d2d1d: Mounted from library/adoptopenjdk
cc4590d6a718: Mounted from library/adoptopenjdk
latest: digest: sha256:e2b63300fa13d3913c7292bad63d4392e2e3042c21ac851e03e813c30f44cf16 size: 1788
```
