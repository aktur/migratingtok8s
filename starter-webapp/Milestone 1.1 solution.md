## Running the Legacy Application

Solution for MacOS:

###Install Apache Tomcat - Make sure you have Java version 8 or above###

install:
```
brew install tomcat
```
check if installed and running
```
brew services list
```
and, if  not, start it
```
brew services tomcat start
```

###Install and configure MySQL###

install:
```
brew install mysql
```
check if installed and running
```
brew services list
```
and, if  not, start it
```
brew services mysql start
```
create a database and a user that can read/write for that database...
```
mysql -uroot
```
once in mysql...
```
create database profiles;
CREATE USER empuser@'localhost' IDENTIFIED BY 'password';
CREATE USER empuser@'%' IDENTIFIED BY 'password';
ALTER USER 'empuser'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
ALTER USER 'empuser'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON profiles.* to empuser@'localhost';
GRANT ALL PRIVILEGES ON profiles.* to empuser@'%';
```

3. Reconfigure for local file system - The images that are uploaded to the application are stored in the file system. Also, there is a logger configured for logging database trace messages. Both are currently configured to point to */tmp*, which will work for MacOS and Linux, but not Windows
4. Reconfigure the MySQL URL and username/password in the webapp configuration
5. Deploy the application in tomcat. See _Resources_ for information on how to do this
```
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password -d mysql/mysql-server:latest
```

## Connect to the docker instance (to run *mysql* command line interface)
```
docker exec -it <34879c7c6016> /bin/bash
```

## Creat database and user in this MySQL (using password *password* configured above)
```
mysql -u root -p <<EOF
EOF
```
