### Converting the Application to a RESTful API

__Deliverables__

1. Console output of the following *httpie* and/or *curl* commands:

```
http POST localhost:8080/profile username=unamerkel password=changeme \
    firstName=Una lastName=Merkel email=unamerkel@example.com   ### create new profile

http localhost:8080/profile/unamerkel   ### fetch user profile (as JSON)

http localhost:8080/profile/russcolombo   ### fetch non-existent user profile (returns 404)

curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@una_merkel.jpg" http://localhost:8080/profile/unamerkel/image  ### upload image (this only works with curl)

http localhost:8080/profile/unamerkel/image  ### download image file (will return a 200 and a message "NOTE: binary data not shown in terminal")
```

__Solution__

Not much needed to be changed in the _pom.xml_, only removal of the *spring-boot-starter-thymeleaf* dependency.

The following files and directories should be removed:
 - config/WebInitializer.java
 - web/HomeController.java
 - web/WebConfig.java
 - static.css
 - anything with **WEB-INF** in its name
 - the resource bundles

The _ProfileController.java_ class needs a fair bit of updating - the business logic in it stays the same, but the wrappers around that change from adding attributes to a model (that then gets forwarded to the front-end/thymeleaf) to marshalling and unmarshalling business objects with JSON

An example of the log from the _http_ and _curl_ commands is here.

```
➜  http POST localhost:8080/profile username=unamerkel password=changeme \
    firstName=Una lastName=Merkel email=unamerkel@example.com   ### create new profile

HTTP/1.1 200
Connection: keep-alive
Content-Type: application/json
Date: Wed, 19 Feb 2020 00:30:42 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

{
    "email": "unamerkel@example.com",
    "firstName": "Una",
    "id": 10,
    "imageFileContentType": null,
    "imageFileName": null,
    "lastName": "Merkel",
    "password": "changeme",
    "username": "unamerkel"
}

➜  http localhost:8080/profile/unamerkel   ### fetch user profile (as JSON)

HTTP/1.1 200
Connection: keep-alive
Content-Type: application/json
Date: Wed, 19 Feb 2020 00:30:57 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

{
    "email": "unamerkel@example.com",
    "firstName": "Una",
    "id": 10,
    "imageFileContentType": null,
    "imageFileName": null,
    "lastName": "Merkel",
    "password": "changeme",
    "username": "unamerkel"
}

➜  http localhost:8080/profile/russcolombo   ### fetch non-existent user profile (returns 404)

HTTP/1.1 404
Connection: keep-alive
Content-Length: 0
Date: Wed, 19 Feb 2020 00:31:08 GMT
Keep-Alive: timeout=60


➜  curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@una_merkel.jpg" \
     http://localhost:8080/profile/unamerkel/image
HTTP/1.1 100

HTTP/1.1 200
Content-Type: text/plain;charset=UTF-8
Content-Length: 42
Date: Wed, 19 Feb 2020 00:44:47 GMT

You successfully uploaded 'una_merkel.jpg'%

➜  http localhost:8080/profile/unamerkel/image
HTTP/1.1 200
Connection: keep-alive
Content-Length: 23007
Content-Type: image/jpeg
Date: Wed, 19 Feb 2020 00:45:34 GMT
Keep-Alive: timeout=60



+-----------------------------------------+
| NOTE: binary data not shown in terminal |
+-----------------------------------------+
```

And a working **pom.xml** is here:

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.4.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>com.manning.pl</groupId>
    <artifactId>profile-mvc</artifactId>
    <version>1.0</version>
    <name>Profile Management</name>
    <description>Profile management</description>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.springdoc</groupId>
            <artifactId>springdoc-openapi-ui</artifactId>
            <version>1.2.30</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <version>1.2</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.5</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.9</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-test</artifactId>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```