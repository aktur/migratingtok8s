### Deploy Application and Services to Kubernetes : Solution

The following set of files show one way to successfully deploy the profile application and supporting services. These *yaml* files are numbered in order of execution, so that they can be run from the directory in which they reside with a single command:
```
kubectl apply -f .
```
where the **.** represents the current directory

__Workflow Solutions:__

Namespace: create a namespace to isolate this application from others running in the cluster.

*01-namespace.yaml*
```
apiVersion: v1
kind: Namespace
metadata:
  name: liveproject
```
This also makes for easier management. For instance, you can view everything deployed in the namespace, and only in the namespace, with:
```
kubectl get all -n liveproject
```

Secret: These are used to store passwords. The values need to be base64-encoded, so a shell script is used to create this yaml

*create_secret.sh*
```
ROOTUSER=`echo -n 'admin' | base64`
USER=`echo -n 'empuser' | base64`
PASS=`echo -n 'password' | base64`
cat > 02-secret.yaml <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: mysqlroot
  namespace: liveproject
type: Opaque
data:
  username: $ROOTUSER
  password: $PASS
---
apiVersion: v1
kind: Secret
metadata:
  name: mysqluser
  namespace: liveproject
type: Opaque
data:
  username: $USER
  password: $PASS
EOF
```
which generates:

*02-secret.yaml*
```
apiVersion: v1
kind: Secret
metadata:
  name: mysqlroot
  namespace: liveproject
type: Opaque
data:
  username: LW4gYWRtaW4K
  password: LW4gcGFzc3dvcmQK
---
apiVersion: v1
kind: Secret
metadata:
  name: mysqluser
  namespace: liveproject
type: Opaque
data:
  username: LW4gZW1wdXNlcgo=
  password: LW4gcGFzc3dvcmQK
```

PersistentVolumeClaims: these are the permanent storage spaces used by the dattabase and the application (for images)

*03-pvcs.yaml*
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pv-claim
  namespace: liveproject
  labels:
    app: profile
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: profile-pv-claim
  namespace: liveproject
  labels:
    app: profile
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
```

Services: the next two yaml files define the services that will front-end the database and application. Services provide a level of indirection to your application that allow you to do such things as scaling and blue-green deployments

*04-mysql-service.yaml*
```
apiVersion: v1
kind: Service
metadata:
  name: profile-mysql
  namespace: liveproject
  labels:
    app: profile
spec:
  ports:
    - port: 3306
  selector:
    app: profile
    tier: mysql
```

*05-profile-service.yaml*
```
apiVersion: v1
kind: Service
metadata:
  name: profile-app
  namespace: liveproject
  labels:
    app: profile
spec:
  ports:
    - port: 8080
  selector:
    app: profile
    tier: service
```

Deployments: We're using deployments here to manage the lifecycle of the the mysql server and application. By using deployments Kubernetes will restart if there are issues. Note the environment variables we set for each deployment
*06-mysql-deployment.yaml*
```
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: profile-mysql
  namespace: liveproject
  labels:
    app: profile
spec:
  selector:
    matchLabels:
      app: profile
      tier: mysql
  template:
    metadata:
      labels:
        app: profile
        tier: mysql
    spec:
      containers:
      - image: grafpoo/empmysql
        name: mysql
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: password
        - name: MYSQL_USER
          value: empuser
        - name: MYSQL_PASSWORD
          value: password
        - name: MYSQL_DATABASE
          value: "profiles"
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-pv-claim
```

*07-profile-deployment.yaml*
```
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: profile
  namespace: liveproject
  labels:
    app: profile
spec:
  selector:
    matchLabels:
      app: profile
      tier: service
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: profile
        tier: service
    spec:
      containers:
      - image: grafpoo/profile-app
        name: profile
        env:
        - name: SPRING_DATASOURCE_URL
          value: jdbc:mysql://profile-mysql:3306/profiles
        - name: SPRING_DATASOURCE_USERNAME
          value: root
        - name: SPRING_DATASOURCE_PASSWORD
          value: password
        ports:
        - containerPort: 8080
          name: profile
        volumeMounts:
        - name: profile-persistent-storage
          mountPath: /home
      volumes:
      - name: profile-persistent-storage
        persistentVolumeClaim:
          claimName: profile-pv-claim
```

To set up autoscaling, I actually first ran it by hand to the the yaml needed:
```
kubectl autoscale deployment profile --cpu-percent=50 --min=1 --max=10 -n liveproject
```
I then trimmed off the current-running-state entries and was left with this:
*09-autoscale.yaml*
```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: profile
  name: profile
  namespace: liveproject
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: profile
      tier: service
  strategy:
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: profile
        tier: service
    spec:
      containers:
      - env:
        - name: SPRING_DATASOURCE_URL
          value: jdbc:mysql://profile-mysql:3306/profiles
        - name: SPRING_DATASOURCE_USERNAME
          value: root
        - name: SPRING_DATASOURCE_PASSWORD
          value: password
        image: grafpoo/profile-app
        imagePullPolicy: Always
        name: profile
        ports:
        - containerPort: 8080
          name: profile
          protocol: TCP
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /home
          name: profile-persistent-storage
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      volumes:
      - name: profile-persistent-storage
        persistentVolumeClaim:
          claimName: profile-pv-claim
```
