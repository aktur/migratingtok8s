### Bootifying the Application

__Deliverables__

1. Console output of the *java -jar* execution. You can copy-paste, or use the *script* command if on Linux or MacOS. And there are probably other ways to capture console output that I haven't thought of.
2. pom.xml of completed project
3. Screenshot of the profile page

__Solution__

#### Console output

You should see the Spring Boot ASCII art header

![alt text](1.2-log-spring-boot.png "Spring Boot logo")

plus other logging, with a message at teh end signifying the application is up and ready

![alt text](1.2-log-app-ready.png "MySQL results")

(full log available in solutions directory)

#### pom.xml

The pom.xml should be bootified. Particularly, the spring boot parent pom should be used, as seen here:

![alt text](1.2-pom.png "Excerpt from pom.xml")

(full _pom.xml_ available in solutions directory)

#### Webapp screenshot

The screenshot of the application running should look the same as in step 1.1. As mentioned before, the URL itself should be different, omitting the servlet engine context.

![alt text](1.2-profile-page.png "Tomcat manager")
