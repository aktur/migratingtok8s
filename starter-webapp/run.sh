mvn clean install || exit 1
docker build -t m2k8s:rest .
docker ps
docker stop m2k8s_boot
docker run -p 8080:8080 --rm --name m2k8s_boot m2k8s:rest
docker ps

